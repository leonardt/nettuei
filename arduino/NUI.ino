//Software for the NUI Project

#include <Servo.h>

//Used for delay
unsigned long CommunicatePreviousMillis = 0;
const long CommunicateInterval = 30;



//Variables for Unity Communication
int forward = 0;
int left = 0;
int right = 0;
int backward = 0;

int forwardSpeed = 0;
int leftSpeed = 0;
int rightSpeed = 0;
int backwardSpeed = 0;


int forwardOld = 0;
int leftOld = 0;
int rightOld = 0;
int backwardOld = 0;

int forwardSpeedOld = 0;
int leftSpeedOld = 0;
int rightSpeedOld = 0;
int backwardSpeedOld = 0;



int forwardResistance = 0;
int leftResistance = 0;
int rightResistance = 0;
int backwardResistance = 0;

bool debugLog = false;

String inputFromUnity = "EMPTY";



//Internal Variables

Servo servoForward;
Servo servoLeft;
Servo servoRight;
Servo servoBackward;

int potforward = A0;
int potleft = A2;
int potright = A3;
int potback = A1;


int potforwardlowest = analogRead(potforward);
int potforwardhightest = analogRead(potforward);

int potleftlowest = analogRead(potleft);
int potlefthightest = analogRead(potleft);

int potrightlowest = analogRead(potright);
int potrighthightest = analogRead(potleft);

int potbacklowest = analogRead(potback);
int potbackhightest = analogRead(potleft);

//current and target positions are used to allow smoth servo movement
int servoforwardtarget = 150;
int servoforwardcurrent = 100;
int servolefttarget = 150;
int servoleftcurrent = 100;
int servorighttarget = 150;
int servorightcurrent = 100;
int servobacktarget = 150;
int servobackcurrent = 100;


//Vorerige Positionen für Speed Berechnung
int forwardPrevPos = 0;
int leftPrevPos = 0;
int rightPrevPos = 0;
int backwardPrevPos = 0;


unsigned long ServoPreviousMillis = 0;
const long ServoInterval = 4;
int servoSteps = 3;

unsigned long SpeedPreviousMillis = 0;
const long SpeedInterval = 50;

unsigned long ReCalibratePreviousMillis = 5000;
const long ReCalibrateInterval = 1000;
int ReCalibrateMaxValue = 30;

unsigned long currentMillis = millis();


void setup() {
  Serial.begin(9600);
  if (debugLog) {
    Serial.println("start setup");
  }
  delay(50);

  if (debugLog) {
    Serial.println("write servos");
  }
  servoForward.write(servoforwardcurrent);
  servoLeft.write(servoleftcurrent);
  servoRight.write(servorightcurrent);
  servoBackward.write(servobackcurrent);
  delay(500);

  if (debugLog) {
    Serial.println("attach servos");
  }
  servoForward.attach(3);  // attaches the servo on pin 9 to the servo object
  delay(500);
  servoLeft.attach(6);
  delay(500);
  servoRight.attach(5);
  delay(500);
  servoBackward.attach(9);
  delay(1500);





  potforwardlowest = analogRead(potforward);
  potforwardhightest = analogRead(potforward);

  potleftlowest = analogRead(potleft);
  potlefthightest = analogRead(potleft);

  potrightlowest = analogRead(potright);
  potrighthightest = analogRead(potright);

  potbacklowest = analogRead(potback);
  potbackhightest = analogRead(potback);



  if (debugLog) {
    Serial.println("setup done");
  }


  delay(15);
}

void loop() {
  currentMillis = millis();



  //Send to Unity
  if (currentMillis - CommunicatePreviousMillis >= CommunicateInterval && currentMillis > 10000) {
    CommunicatePreviousMillis = currentMillis;


    if (forward != forwardOld || left != leftOld || right != rightOld || backward != backwardOld || forwardSpeed != forwardSpeedOld || leftSpeed != leftSpeedOld || rightSpeed != rightSpeedOld || backwardSpeed != backwardSpeedOld) {
      //Something Changed
      String myPosOutput = String(forward) + ";" + String(right) + ";" + String(backward) + ";" + String(left);
      String mySpeedOutput = String(forwardSpeed) + ";" + String(rightSpeed) + ";" + String(backwardSpeed) + ";" + String(leftSpeed);

      String myOutput = myPosOutput + ";" +  mySpeedOutput;
      Serial.println(myOutput);

      forwardOld = forward;
      leftOld = left;
      rightOld = right;
      backwardOld = backward;

      forwardSpeedOld = forward;
      leftSpeedOld = left;
      rightSpeedOld = right;
      backwardSpeedOld = backward;

    } else {
      //Nothing Changed
      //Serial.println("$");
    }
  }








  //Receive from Unity
  if (Serial.available() > 0) {
    inputFromUnity = String(Serial.readString());

    int myDivider[3];
    int myDividerCounter = 0;

    for (int i = 0; i != inputFromUnity.length(); i++)
      if (inputFromUnity.charAt(i) == ';') {
        myDivider[myDividerCounter] = i;
        myDividerCounter++;
      }
    String myString;

    myString = inputFromUnity.substring(0, myDivider[0]);
    forwardResistance = myString.toInt();

    myString = inputFromUnity.substring(myDivider[0] + 1, myDivider[1]);
    rightResistance = myString.toInt();

    myString = inputFromUnity.substring(myDivider[1] + 1, myDivider[2]);
    backwardResistance = myString.toInt();

    myString = inputFromUnity.substring(myDivider[2] + 1, inputFromUnity.length());
    leftResistance = myString.toInt();



  }












  //Map Inputs


  //Forward
  if (analogRead(potforward) > potforwardhightest) {
    potforwardhightest = analogRead(potforward);
    if (debugLog) {
      Serial.println("forward new high");
    }
  }

  if (analogRead(potforward) < potforwardlowest) {
    potforwardlowest = analogRead(potforward);
    if (debugLog) {
      Serial.println("forward new low");
    }
  }

  forward = map(analogRead(potforward), potforwardlowest, potforwardhightest, 0, 100);



  //Left
  if (analogRead(potleft) > potlefthightest) {
    potlefthightest = analogRead(potleft);
    if (debugLog) {
      Serial.println("left new high");
    }
  }

  if (analogRead(potleft) < potleftlowest) {
    potleftlowest = analogRead(potleft);
    if (debugLog) {
      Serial.println("left new low");
    }
  }

  left = map(analogRead(potleft), potleftlowest, potlefthightest, 0, 100);



  //Right
  if (analogRead(potright) > potrighthightest) {
    potrighthightest = analogRead(potright);
    if (debugLog) {
      Serial.println("right new high");
    }
  }

  if (analogRead(potright) < potrightlowest) {
    potrightlowest = analogRead(potright);
    if (debugLog) {
      Serial.println("right new low");
    }
  }

  right = map(analogRead(potright), potrightlowest, potrighthightest, 0, 100);



  //back
  if (analogRead(potback) > potbackhightest) {
    potbackhightest = analogRead(potback);
    if (debugLog) {
      Serial.println("back new high");
    }
  }

  if (analogRead(potback) < potbacklowest) {
    potbacklowest = analogRead(potback);
    if (debugLog) {
      Serial.println("back new low");
    }
  }

  backward = map(analogRead(potback), potbacklowest, potbackhightest, 0, 100);






  //Speed calculation
  if (currentMillis - SpeedPreviousMillis >= SpeedInterval) {
    SpeedPreviousMillis = currentMillis;

    forwardSpeed = forward - forwardPrevPos;
    forwardPrevPos = forward;

    leftSpeed = left - leftPrevPos;
    leftPrevPos = left;

    rightSpeed = right - rightPrevPos;
    rightPrevPos = right;

    backwardSpeed = backward - backwardPrevPos;
    backwardPrevPos = backward;
  }









  /*
    Serial.print("F pos = ");
    Serial.print(forward);
    Serial.print(" Speed = ");
    Serial.print(forwardSpeed);

    Serial.print(" --- L pos = ");
    Serial.print(left);
    Serial.print(" Speed = ");
    Serial.print(leftSpeed);

    Serial.print(" --- R pos = ");
    Serial.print(right);
    Serial.print(" Speed = ");
    Serial.print(rightSpeed);

    Serial.print(" --- B pos = ");
    Serial.print(backward);
    Serial.print(" Speed = ");
    Serial.println(backwardSpeed);
  */




  //ReCalibrate
  if (currentMillis - ReCalibratePreviousMillis >= ReCalibrateInterval) {
    ReCalibratePreviousMillis = currentMillis;

    if (forward < ReCalibrateMaxValue && forward > 0 && forwardSpeed < 2 && forwardSpeed > -2) {
      if (debugLog) {
        Serial.println("ReCalibrate forward");
      }
      potforwardlowest = potforwardlowest + 1;
    }

    if (left < ReCalibrateMaxValue && left > 0 && leftSpeed < 2 && leftSpeed > -2) {
      if (debugLog) {
        Serial.println("ReCalibrate left");
      }
      potleftlowest = potleftlowest + 1;
    }

    if (right < ReCalibrateMaxValue && right > 0 && rightSpeed < 2 && rightSpeed > -2) {
      if (debugLog) {
        Serial.println("ReCalibrate right");
      }
      potrightlowest = potrightlowest + 1;
    }

    if (backward < ReCalibrateMaxValue && backward > 0 && backwardSpeed < 2 && backwardSpeed > -2) {
      if (debugLog) {
        Serial.println("ReCalibrate backward");
      }
      potbacklowest = potbacklowest + 1;
    }
  }


  //Calculate Servo Positions

  //forward
  servoforwardtarget = 195 - (forward * 1.7);
  int servoNormalized = map(servoforwardtarget, 0, 195, 100, 0);
  servoforwardtarget = servoforwardtarget + forwardResistance / 1.5;
  // servoforwardtarget = servoforwardtarget + ((servoNormalized - forward) * (forwardResistance)) + forwardResistance;

  //left
  servolefttarget = 195 - (left * 1.7);
  servoNormalized = map(servolefttarget, 0, 195, 100, 0);
  servolefttarget = servolefttarget + leftResistance / 3;



  //right
  servorighttarget = 195 -  (right * 1.7);
  servoNormalized = map(servorighttarget, 0, 195, 100, 0);
  servorighttarget = servorighttarget + rightResistance / 3;


  /*
    Serial.print(servorighttarget);
    Serial.print(" - ");
    Serial.print(right);
    Serial.print(" - ");
    Serial.println(servoNormalized);
  */

  //back
  servobacktarget = 180 - (backward * 1.75);
  servoNormalized = map(servobacktarget, 0, 180, 100, 0);
  servobacktarget = servobacktarget + backwardResistance / 3;




  // Serial.print(servobacktarget);
  // Serial.print(" - - ");
  // Serial.println(backward);

  //override before manual caibration
  if (currentMillis < 10000) {
    if (currentMillis > 7000) {
      servoforwardtarget = 180;
      servolefttarget = 180;
      servorighttarget = 180;
      servobacktarget = 180;
    } else {
      servoforwardtarget = 0;
      servolefttarget = 0;
      servorighttarget = 0;
      servobacktarget = 0;
    }
  }





  //Update Servo Positions
  if (currentMillis - ServoPreviousMillis >= ServoInterval) {
    //Serial.println("update Servo Pos");
    ServoPreviousMillis = currentMillis;

    for (int i = 0; i != servoSteps; i++) {

      if (servoforwardcurrent < servoforwardtarget) {
        servoforwardcurrent = servoforwardcurrent + 1;
      }

      if (servoforwardcurrent > servoforwardtarget) {
        servoforwardcurrent = servoforwardcurrent - 1;
      }



      if (servoleftcurrent < servolefttarget) {
        servoleftcurrent = servoleftcurrent + 1;
      }

      if (servoleftcurrent > servolefttarget) {
        servoleftcurrent = servoleftcurrent - 1;
      }


      if (servorightcurrent < servorighttarget) {
        servorightcurrent = servorightcurrent + 1;
      }

      if (servorightcurrent > servorighttarget) {
        servorightcurrent = servorightcurrent - 1;
      }


      if (servobackcurrent < servobacktarget) {
        servobackcurrent = servobackcurrent + 1;
      }

      if (servobackcurrent > servobacktarget) {
        servobackcurrent = servobackcurrent - 1;
      }
    }
  }




  servoForward.write(servoforwardcurrent);
  servoLeft.write(servoleftcurrent);
  servoRight.write(servorightcurrent);
  servoBackward.write(servobackcurrent);
}

