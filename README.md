# Analoge Tastatur

## Inhaltsverzeichnis
- [Projektinformationen](#projektinformationen)<br>
  - [Idee](#idee)<br>
  - [Interaktive Elemente](#interaktive-elemente)<br>
  - [Tastatur Prototyp](#tastatur-prototyp)<br>
  - [Arduino-Unity Kommunikation](#arduino-unity-kommunikation)<br>
- [Installation](#installation)<br>
    - [Arduino](#arduino)<br>
    - [Unity](#unity)<br>

## Projektinformationen

### Idee
Die anfängliche Idee war es Spielern über die Tastatur Feedback zur Umgebung und zum aktuellen Laufverhalten zu geben. Durch unterschiedlich starkes herunterdrücken der Taste wird die Bewegungsgeschwindigkeit verändert. Bei erhöhtem Bewegungsaufwand bspw. an Steigungen, im Wasser oder durch Wind bekommen Spieler den erhöhten Aufwand der Fortbewegung rückgemeldet in dem sie die Tasten einerseits für die gleiche Geschwindigkeit stärker herunterdrücken müssen und die Taste gleichzeitig einen stärkeren Widerstand bietet.

<div style="text-align: center;">

![Datenfluss Diagramm](media/datenfluss.svg)
<p>Abbildung 1: Datenfluss</p><br>

</div>

Für das Steuern des Spielers wurde ein Prototypen einer analogen Tastatur (siehe [Abb. 5: Tastatur Prototyp](#tastatur-prototyp)) und eine Umgebung in Unity (siehe [Interaktive Elemente](#interaktive-elemente)) entworfen. Die analogen Eingaben werden verarbeitet und an Unity weitergeleitet (siehe [Arduino-Unity Kommunikation](https://www.alanzucconi.com/2015/10/07/how-to-integrate-arduino-with-unity/)). Bewegt sich der Spieler auf eines der [Interaktiven Elemente](#interaktive-elemente), wird der Widerstand für das Element an den Arduino gesendet, welcher den Druck der Tasten erhöht. Der Spieler kann sich immer noch fortbewegen, jedoch wird ein erhöhter Aufwand benötigt, um die Taste zu drücken. 
<br>
Weitere Ideen, die bisher nicht umgesetzt wurden, sind das Umstoßen von Gegenständen durch ein schnelles herunterdrücken einer Taste, fliegen durch gleichzeitiges Herunterdrücken aller Tasten und ein unsichtbares Labyrinth, dessen Wände nur durch den Widerstand der Tasten zu erkennen sind.

### Interaktive Elemente
![Screenshot Unity Projekt: Rampe](media/rampe.jpeg)
<p style="text-align: center;">Abbildung 2: Rampe</p><br>

![Screenshot Unity Projekt: Rampe + Wasser](media/rampe-wasser.jpeg)
<p style="text-align: center;">Abbildung 3: Rampe und Wasser</p><br>

![Screenshot Unity Projekt: Windkanal](media/windkanal.jpeg)
<p style="text-align: center;">Abbildung 4: Windkanal</p><br>

### Tastatur Prototyp
Die Tastatur verfügt über vier analoge Tasten. Jede Taste verfügt über ein eigenes Potentiometer, um die Drucktiefe zu bestimmen und einen Servomotor, um den Druckwiderstand zu variiern.</br>
Das Potentiometer und die Taste sind über ein Gelenk aus Draht verbunden, das als Übersetzung der Drucktiefe für das Potis dient. Wird eine Taste herunter gedrückt dreht sich das dazugehörige Potentiometer. Die Drehung des Potentiometers gibt also Aufschluss darüber, wie weit die Taste runtergedrückt wurde.</br>
Wird eine Taste runtergerückt, erzeugt der Servo einen Gegendruck, der davon abhängig ist wo sich der Spieler innerhalb der Welt befindet.
Der Gegendruck entsteht indem der Motor der Taste über ein Zahnrad, einen Zylinder nach oben und somit gegen die Taste drückt.

![Screenshot Unity Projekt: Windkanal](media/analoge-tastatur-1.jpg)
<p style="text-align: center;">Abbildung 5: Prototyp von vier analogen Tasten</p><br>

![Screenshot Unity Projekt: Windkanal](media/analoge-tastatur-2.jpg)
<p style="text-align: center;">Abbildung 6: Fokus auf Arduino und Servo</p><br>

### Arduino-Unity Kommunikation
Für die Übermittlung der Tastenstellung und der Geschwindigkeit mit der die Tasten heruntergedrückt, die d er Arduino an Unity sendet und der Widerstände für die einzelnen Tasten, die Unity an den Arduino sendet, wird der Serielle Port als Kommunikationskanal verwendet.

Dabei sendet der Arduino folgenden String in dem acht Werte, die durch ein `;` getrennt werden, enthalten sind. Bei den ersten vier Werten handelt es sich um die Distanz, die die jeweilige Taste heruntergedrückt wurde und bei den letzten vier um die Geschwindigkeit mit der die Taste gedrückt wurde.
```
upKeyPos;rightKeyPos;downKeyPos;leftKeyPos;upKeyVelocity;rightKeyVelocity;downKeyVelocity;leftKeyVelocity
```

Unity sendet ebenfalls die Daten der Widerstände für die einzelnen Tasten durch einen String kodiert an den Arduino. Dabei wird `;` erneut als Trennzeichen verwendet. Der Standardwert für den Widerstand der Tasten beträgt 10. Bewegt sich der Spieler auf ein Interatktives Element ändert sich der Widerstand (Steile Rampe: 40, Flache Rampe: 20, Wasserbecken: 100, Windkanal: 100).
```
resistanceUp;resistanceRight;resistanceDown;resistanceLeft
```

Alle Variablen können Werte im Bereich 0 bis 100 annehmen.


## Installation
Um das Projekt nutzen zu können, müssen folgende Schritte ausgeführt werden:

### Arduino
1. [Arduino IDE](https://www.arduino.cc/en/software) installieren
2. Repository clonen (der Arduino-Code befindet sich in `../arduino`)
3. Der Arduino muss per USB-Kabel mit einem PC verbunden werden.
4. Arduino Code muss auf den Arduino geuploaded werden.
5. Optional: Arduino bei bedarf resetten.

### Unity
1. [Unity](https://unity.com/download#how-get-started) installieren
2. Repository clonen (das Unity-Projekt befindet sich in `./unity`)
3. Im Code den Port angeben über den Unity und der Arduino kommunizieren. Der Port kann in `unity/Assets/StarterAssets/FirsPersonControler/Scripts/Arduino/ArduinoCommunicator.cs` in der Zeile 32 geändert werden:<br>
```
stream = new SerialPort("Port hier einfügen", 9600);
```
4. `./unity` Projekt in Unity über `Open/Add Projekt from disk` öffnen
5. Projekt starten

Die verwendete Szene ist unter `unity/Assets/StarterAssets/FirstPersonControler/Scenes/Playground` zu finden. Skripte für die Kommunikation mit dem Arduino und die Steuerung des Spielers befinden sich im Ordner `unity/Assets/StarterAssets/FirsPersonControler/Scripts/Arduino`.
