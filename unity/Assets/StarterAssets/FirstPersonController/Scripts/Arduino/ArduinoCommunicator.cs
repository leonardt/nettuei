using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.IO;
using System;

public class ArduinoCommunicator : MonoBehaviour
{

SerialPort stream;

public int upKeyPos;
public int rightKeyPos;
public int downKeyPos;
public int leftKeyPos;

public int upKeyVelocity;
public int rightKeyVelocity;
public int downKeyVelocity;
public int leftKeyVelocity;

public string input = "0";
public string inputTemp = "0";

public string output = "0";
public string outputTmp = "0";

    // Start is called before the first frame update
    void Start()
    {
        stream = new SerialPort("/dev/ttyACM0", 9600);
        stream.ReadTimeout = 0;
        stream.Open();
    }

    // Update is called once per frame
    void Update()
    {
        input = ReadFromArduino();
        stream.DiscardInBuffer();
        HandleInput(input);
    }

    public void HandleInput(string s) {

        // decode input string
        // input string from:
        //  upKeyPos;rightKeyPos;downKeyPos;leftKeyPos;upKeyVelocity;rightKeyVelocity;downKeyVelocity;leftKeyVelocity
        Debug.Log("IN: " + s);

        string[] trimmedString = s.Split(";");

        upKeyPos = mapInput(int.Parse(trimmedString[0]));
        rightKeyPos = mapInput(int.Parse(trimmedString[1]));
        downKeyPos = mapInput(int.Parse(trimmedString[2]));
        leftKeyPos = mapInput(int.Parse(trimmedString[3]));

        upKeyVelocity = int.Parse(trimmedString[4]);
        rightKeyVelocity = int.Parse(trimmedString[5]);
        downKeyVelocity = int.Parse(trimmedString[6]);
        leftKeyVelocity = int.Parse(trimmedString[7]);
    }

    private int mapInput(int inValue) {
        if (inValue <= 5) {
            return 0;
        } else if (inValue <= 20) {
            return 1;
        } else if (inValue <= 40) {
            return 2;
        } else if (inValue <= 60) {
            return 3;
        } else if (inValue <= 80) {
            return 5;
        } else if (inValue <= 100) {
            return 7;
        } else {
            return 0;
        }
    }

    public void SendResistance(string pressureUp, string pressureRight, string pressureDown, string pressureLeft){
        Debug.Log("OUT: " + pressureUp + ";" + pressureRight + ";" + pressureDown + ";" + pressureLeft);
        
        output = pressureUp + ";" + pressureRight + ";" + pressureDown + ";" + pressureLeft;

        if (output != outputTmp) {
            WriteToArduino(output);
            outputTmp = output;
        }
    }

    public void WriteToArduino(string message) {
        stream.WriteLine(message);
        stream.BaseStream.Flush();
    }

    public string ReadFromArduino (int timeout = 0) {
        stream.ReadTimeout = timeout;        
        try {
            return stream.ReadLine();
        }
        catch (TimeoutException e) {
            Debug.Log(e);
            return null;
        }
    }

    public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity) {
        DateTime initialTime = DateTime.Now;
        DateTime nowTime;
        TimeSpan diff = default(TimeSpan);
        string dataString = null;
        do {
            try {
                dataString = stream.ReadLine();
            }
            catch (TimeoutException) {
                dataString = null;
            }
            if (dataString != null)
            {
                callback(dataString);
                yield break; // Terminates the Coroutine
            } else
                yield return null; // Wait for next frame
            nowTime = DateTime.Now;
            diff = nowTime - initialTime;
        } while (diff.Milliseconds < timeout);
        if (fail != null)
            fail();
        yield return null;
    }
}
